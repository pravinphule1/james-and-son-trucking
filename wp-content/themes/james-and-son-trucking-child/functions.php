<?php
/**
 * James And Son Trucking Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package James And Son Trucking
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_JAMES_AND_SON_TRUCKING_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'james-and-son-trucking-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_JAMES_AND_SON_TRUCKING_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );